import {applyMiddleware, createStore} from 'redux'
import reducer from './reducers/index'
import {createLogger} from 'redux-logger'
import thunk from 'redux-thunk'
import React from 'react'
import ReactDOM from 'react-dom'
import {Provider} from 'react-redux'

import ItemList from './components/ItemList'
import './bootstrap/css/style.css';

const middleware = applyMiddleware(thunk, createLogger());

const store = createStore(reducer, middleware);
/*
store.subscribe(function (){
  console.log("Store changed", store.getState())
})
*/


//store.dispatch({type: "LOAD_USERS_DATA", payload: {users: usersList}});


ReactDOM.render(<Provider store={store}>
  <div>
    <ItemList />
  </div>
</Provider>, document.getElementById('app'));