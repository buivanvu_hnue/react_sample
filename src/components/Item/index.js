import React, {Component, PropTypes} from 'react'

export default class Item extends Component {
    constructor(props) {
        super(props);
    }
    
    itemClick(){
        this.setState({
            checked: true
        })
    }

    render(){
        const {name, count, onClick, checked} = this.props;
        let itemClass = checked || this.state && this.state.checked ? "item checked" : "item";
        return (<div onClick={this.itemClick.bind(this)} className={itemClass}>Name: {name} <span className="label label-primary pull-right"> Count: {count} </span> </div>)
    }
}