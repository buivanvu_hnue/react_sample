import React, {Component} from 'react'
import PropTypes from 'prop-types';
import {connect} from 'react-redux'
import Item from '../Item'

class AddNewItem extends Component {

    clickToAdd(){
        return this.props.onClick(this.inputName.value, this.inputCount.value);
    }

    render(){
        let clearStyle = {
            clear: "both"
        }
        return (
            <div className="row-fluid add-new-form">
                <div className="form-group col-md-4">
                Name:
                <input ref={(input) => { this.inputName = input; }} className="form-control" />
                </div>
                <div className="form-group col-md-4">
                Count
                <input ref={(input) => { this.inputCount = input; }} className="form-control" />
                </div>
                <div className="form-group col-md-3">
                <div> </div>
                <button style={{marginTop: '20px'}} onClick = {this.clickToAdd.bind(this)}  className="btn btn-danger">Add new Item</button>
                </div>
                <div style={clearStyle}></div>
            </div>
        )
    }
}

class ItemList extends Component {
    constructor(props) {
        super(props);
    }
    
    static propTypes = {
        list: PropTypes.array.isRequired,
    }

    render(){
        const itemList = this.props.list;
        let clickToAdd = (name, count) => {
            this.props.dispatch({
                type: "ADD_NEW_ITEM",
                payload: {
                    name,
                    count
                }
            })
        }

        let style = {
            width: '600px',
            marginLeft: '100px',
            marginTop: '30px'
        }
        return (
        <div style={style} >
            <div className="item-list">
                {
                    itemList.map( (elm, index) => 
                        <Item key={index} {...elm} />
                    )
                }
            </div>
            <AddNewItem onClick={clickToAdd} />
        </div>
        )
    }
}

export default connect(
    (state) => ({
        list:  state.items
    })
)(ItemList)