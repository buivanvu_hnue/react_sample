let initState = {}

export default function (state = initState, action){
    let newState = {state}
    switch (action.type) {
        case 'LOAD_USERS_DATA': {
            newState = action.payload.users;
            return newState;
        }
        case 'ADD_NEW_USER': {
            let users = [];
            if (state.length) {
                users = [...state];
            }
            users.push(action.payload.user);
            console.log(users);
            return users;
        }
    }
    return newState;
}