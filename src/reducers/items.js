let initState  = [
  { name: "Car from JAPAN", count: 15, checked: false },
  { name: "Car from VIETNAM", count: 23, checked: false },
  { name: "Car from LAO", count: 1, checked: false },
  { name: "Car from CAMPUCHIA", count: 5, checked: false },
  { name: "Car from HANOi", count: 10, checked: false }
]
export default function (state = initState, action){
    console.log("reducer items"); console.log(state);
    switch (action.type) {
        case 'ADD_NEW_ITEM': {
            return [...state, {...action.payload, checked: true}];
        }
        default:
        return state;
    }
    return state;
}
