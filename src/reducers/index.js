import {combineReducers} from 'redux'
import users from './user'
import tweet from './tweet'
import items from './items'

export default combineReducers({
    users,
    items
})